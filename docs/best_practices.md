# Best practices

This page will detail a set of best practices when using LibVLC/LibVLCSharp

[Back](home.md)

TODO:
- debugging with libvlc logs,
- Dispose() libvlc objects as necessary
- check if the issue is present on official VLC apps
- try multiple on platforms and versions
- try multiple samples
- etc.
